# MDT-Bildklassifikation

Code pullen:

git clone git@gitlab.com:phil_wdl/mdt-bildklassifikation.git

Code pushen:

- git add .
- git commit -m "*commit Message*"
- git push

## Hinweise zum ausführen des Docker Containers

### (Bash Shell notwendig)

Docker Image bauen:
- bash docker-build.sh

Docker Image ausführen:
- bash docker-run.sh

Die Anwendung kann ggf. *nicht* unter der angegeben IP-Adresse im Terminal (0.0.0.0:8080) aufgerufen werden.
Die lokale IP-Adresse (0.0.0.0) muss durch die IP-Adresse des Docker Containers ersetzt werden (meist: 172:17.0.2).
Diese kann aber durch folgenden Befehl auch im Terminal angezeigt werden: - sudo docker container inspect [CONTAINER NAME ODER ID]

Somit kann die Anwendung unter folgender IP-Adresse aufgerufen werden [DOCKER IP_ADRESSE]:8080 (meist: 172:17.0.2:8080).

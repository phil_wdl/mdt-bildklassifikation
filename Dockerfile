FROM python:3.8-slim-buster
RUN apt update
RUN mkdir -p /app/
COPY anforderungen_pip.txt anforderungen_sudo.txt app/ /app/
RUN pip install -r /app/anforderungen_pip.txt
RUN apt-get update && apt-get install -y python3-tk libgl1-mesa-glx libglib2.0-0
WORKDIR /app/
CMD ["uvicorn", "src.main:app", "--host", "172.17.0.2", "--port", "8080" ] 

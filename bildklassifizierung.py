import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import cv2
import tkinter as tk
from tkinter import filedialog
import pickle


modeliste = keras.datasets.fashion_mnist;

(train_images, train_labels), (test_images, test_labels) = modeliste.load_data()

class_names = ['T-shirt/top', 'Sneaker', 'Pullover', 'Ankle boot', 'Coat', 'Sandal', 'Shirt', 'Trouser', 'Bag', 'Dress']

train_images.shape 

plt.figure()
plt.imshow(train_images[0])
plt.colorbar()
plt.grid(False)
plt.show()

train_images = train_images / 255.0
test_images = test_images / 255.0

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])
plt.show()

# setup the layer
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10)
])

#Fitting the Model
model.compile(optimizer='adam', loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True))
model.fit(train_images, train_labels, epochs=10)
test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)
test_acc1 = model.evaluate(test_images,  test_labels, verbose=2) 
""" vor test_acc1 müsste laut Tutorium test_loss stehen, allerdings funktioniert dann der Code nicht mehr -> TypeError: cannot unpack non-iterable float objectS"""
print('\nTest accuracy:', test_acc1)
model.fit(train_images, train_labels, epochs=10)
#Evaluating Accuracy

test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)
print("bisDA-TEST1")

#Fitting the Model
model.fit(train_images, train_labels, epochs=10)
#Evaluating Accuracy
test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)

with open("anwendung/quellen/model.pickle", "wb") as f:
    pickle.dump(model, f)

with open("anwendung/quellen/class_names.pickele", "wb") as f:
    pickle.dump(class_names, f)


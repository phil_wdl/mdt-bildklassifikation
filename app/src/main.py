
import pickle
import numpy as np
import cv2
import tkinter as tk
from tkinter import filedialog
from fastapi import FastAPI
import os







with open(os.path.join(os.path.dirname(__file__),'model.pickle'), 'rb') as f:
    model = pickle.load(f)

# Laden der Klassennamen aus class_names.pickle
with open(os.path.join(os.path.dirname(__file__),'class_names.pickle'), 'rb') as f:
    class_names = pickle.load(f)



class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()
    
    def create_widgets(self):
        self.quit = tk.Button(self, text="Quit", command=self.master.destroy)
        self.quit.pack(side="bottom")
        
        self.open_image = tk.Button(self, text="Open Image", command=self.open_img)
        self.open_image.pack(side="bottom")
        
        self.label = tk.Label(self, text="")
        self.label.pack(side="bottom")

    def open_img(self):
        
        filename = tk.filedialog.askopenfilename(initialdir = "/", title = "Select a file", filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        beispielbild = cv2.imread(filename)


        beispielbild = cv2.resize(beispielbild, ( 28, 28))
        beispielbild = cv2.cvtColor(beispielbild, cv2.COLOR_BGR2GRAY)
        beispielbild = np.reshape(beispielbild, (28, 28)) 
        beispielbild = beispielbild / 255.0
        vorhersage = model.predict(beispielbild[np.newaxis, ..., np.newaxis])
        gesuchtes_Label = np.argmax(vorhersage[0])
        self.label["text"] = class_names[gesuchtes_Label]
            



app = FastAPI()

@app.get("/")
def start_gui(): 
    root = tk.Tk()
    root.geometry("400x400")
    root.title("MDT - Bildklassifikation")
    app = Application(master=root)
    app.mainloop()


if __name__ == "__main__":
    start_gui()
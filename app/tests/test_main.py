from fastapi.testclient import TestClient

from src.main import app
from src.main import DataOut

client = TestClient(app)


def test_predict():
    request = {
        "review": [
            "very nice"
        ]
    }
    response = client.post("/predict", json=request)
    assert response.status_code == 200
    result = DataOut(**response.json())
    assert result.prediction == ["positive"]